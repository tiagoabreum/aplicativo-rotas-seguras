package com.unicamp.luiz.rotasseguras.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.unicamp.luiz.rotasseguras.Activities.MainActivity;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;
import com.unicamp.luiz.rotasseguras.Activities.StepsListActivity;
import com.unicamp.luiz.rotasseguras.Fragments.RoutesListFragment;
import com.unicamp.luiz.rotasseguras.Models.Place;
import com.unicamp.luiz.rotasseguras.R;
import com.unicamp.luiz.rotasseguras.Tasks.GetAutoSuggestionsTask;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class SearchAutoComplete implements  MaterialSearchBar.OnSearchActionListener,
        NavigationView.OnNavigationItemSelectedListener, java.io.Serializable {

    private MaterialSearchBar searchBar;
    private final MapsActivity mapsActivity;
    private RecyclerView searchrv;
    private final Activity activity;
    private DrawerLayout drawer;
    private final GoogleMap mMap;
    private final Context context;
    private static final String TAG = "PlaceAutocompleteAdapter";
    private ArrayList<AutocompletePrediction> mResultList;
    private List<List<HashMap<String, String>>> predictions;
    private GetAutoSuggestionsTask getAutoSuggestionsTask;
    private CustomSuggestionsAdapter customSuggestionsAdapter;
    private String request;
    private final SearchAutoComplete searchAutoComplete;
    private final int SEARCH_FROM_SEARCH_BAR = 0;

    public SearchAutoComplete(MapsActivity mapsActivity){
        this.mapsActivity = mapsActivity;
        activity = mapsActivity.getActivity();
        context = mapsActivity.getContext();
        mMap = mapsActivity.getMap();
        searchAutoComplete = this;
        configureLayout();
        addListener();
        addSuggestionListener();
    }


    private void configureLayout(){
        drawer = activity.findViewById(R.id.drawer_layout);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(LAYOUT_INFLATER_SERVICE);
        NavigationView navigationView = activity.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        searchBar = activity.findViewById(R.id.searchBar);
        customSuggestionsAdapter = new CustomSuggestionsAdapter(inflater);
        searchBar.setOnSearchActionListener(this);
        searchBar.setHint(context.getResources().getString(R.string.unicamp));
        searchBar.setCardViewElevation(10);
        searchrv = activity.findViewById(R.id.mt_recycler);

    }

    private void addListener(){
        searchBar.addTextChangeListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                getAutoSuggestionsTask = (GetAutoSuggestionsTask)new GetAutoSuggestionsTask(context,searchAutoComplete,SEARCH_FROM_SEARCH_BAR).execute(editable.toString());
            }

        });
    }

    private void addSuggestionListener() {

        searchrv.addOnItemTouchListener(new RecyclerTouchListener(context, searchrv, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                    Place mPlace = customSuggestionsAdapter.getSuggestions().get(position);
                    String title = mPlace.getTitle();
                    String subtitle = mPlace.getSubtitle();
                    String privateId = searchForId(title,subtitle);
                    if(privateId!= null){
                        privateId = GetAutoSuggestionsTask.requestPlaceWithId(privateId);
                        onSearchConfirmed(privateId);
                    }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    private String searchForId(String place, String subtitle) {

        if(predictions!= null){
                for(int i=0;i< predictions.size();i++){
                        String s = predictions.get(i).get(0).get("description");
                        String t = predictions.get(i).get(2).get("secondary_text");
                        if(s.contains(place) && t.contains(subtitle)){
                            return predictions.get(i).get(1).get("place_id");
                        }
                    }
        }
        return null;
    }

    @Override
    public void onSearchConfirmed(CharSequence text) {
        int SEARCH_FROM_PRIVATEID = 1;
        getAutoSuggestionsTask = (GetAutoSuggestionsTask)new GetAutoSuggestionsTask(context, this, SEARCH_FROM_PRIVATEID).execute(request);
    }

    @Override
    public void onButtonClicked(int buttonCode) {
        switch (buttonCode){
            case MaterialSearchBar.BUTTON_NAVIGATION:
                drawer.openDrawer(Gravity.LEFT);
                break;
            case MaterialSearchBar.BUTTON_SPEECH:
                break;
            case MaterialSearchBar.BUTTON_BACK:
                searchBar.disableSearch();
                break;
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.etapas_navegacao:
                if(mapsActivity.getSteps()!=null){
                    Intent intent = new Intent(activity,StepsListActivity.class);
                    intent.putExtra("steps", (Serializable) mapsActivity.getSteps()); //Optional parameters
                    intent.putExtra("currentRoute",mapsActivity.getRoute().getCurrentRoute());
                    activity.startActivity(intent);
                    closeDrawer();
                }
                closeDrawer();
                return (true);
            case R.id.rotas:
                if(mapsActivity.getRoute()!=null){
                    RoutesListFragment fragment = new RoutesListFragment();
                    Bundle args = new Bundle();
                    args.putSerializable("routes", mapsActivity.getRoute().getRoutes());
                    args.putSerializable("distances", mapsActivity.getRoute().getDistance());
                    args.putSerializable("times", mapsActivity.getRoute().getTime());
                    fragment.setArguments(args);
                    ((MainActivity) activity).getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).addToBackStack(null).commit();
                    mapsActivity.getActivity().findViewById(R.id.zoomIn).setVisibility(View.GONE);
                    mapsActivity.getActivity().findViewById(R.id.zoomOut).setVisibility(View.GONE);
                    mapsActivity.getActivity().findViewById(R.id.current_location).setVisibility(View.GONE);
                    mapsActivity.getActivity().findViewById(R.id.searchBar).setVisibility(View.GONE);
                    mapsActivity.getActivity().findViewById(R.id.card_view_distance).setVisibility(View.GONE);
                    mapsActivity.getActivity().findViewById(R.id.layer_fab_card).setVisibility(View.GONE);
                    closeDrawer();
                }
                closeDrawer();
                return (true);
            case R.id.rota_segura:
                if(mapsActivity.isSearching()){
                    MapsActivity.progressBar.show(mapsActivity.getActivity());
                }else if(mapsActivity.isSearchCompleted()){
                    int safeRoute = mapsActivity.getRoute().getNumRoutes()-1;
                    if(safeRoute>0 && safeRoute!= mapsActivity.getRoute().getCurrentRoute())
                        mapsActivity.getRoutes(safeRoute);
                }
                closeDrawer();
                return  true;
            case R.id.start_navigation:
                if(mapsActivity.isSearching()){
                    MapsActivity.progressBar.show(mapsActivity.getActivity());
                }else if(mapsActivity.isSearchCompleted()){
                    mapsActivity.mapsIntentRequest();
                }
                closeDrawer();
                return true;
            case R.id.padrao:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                Toast.makeText(context, "Modo padrão ativo", Toast.LENGTH_SHORT).show();
                closeDrawer();
                return (true);
            case R.id.satelite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                Toast.makeText(context, "Modo satélite ativo", Toast.LENGTH_SHORT).show();
                closeDrawer();
                return (true);
            case R.id.relevo:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                Toast.makeText(context, "Modo relevo ativo", Toast.LENGTH_SHORT).show();
                closeDrawer();
                return (true);
            case R.id.transito:
                mMap.setTrafficEnabled(!mMap.isTrafficEnabled());
                if(mMap.isTrafficEnabled()){
                    Toast.makeText(context, "Modo com trânsito ativo", Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(context, "Modo com trânsito inativo", Toast.LENGTH_SHORT).show();
                closeDrawer();
                return (true);
        }
        return true;
    }

    private void closeDrawer(){
        DrawerLayout drawer = activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void updateId(String request){
        this.request = request;
    }

    public void updateReturnWithId(LatLng latLng) {
        CardView cardView= activity.findViewById(R.id.card_view_distance);
        cardView.setVisibility(View.GONE);
        activity.findViewById(R.id.layer_fab_card).setVisibility(View.GONE);
        mMap.clear();
        hideKeyboard();
        searchBar.disableSearch();
        searchBar.hideSuggestionsList();
        searchBar.setHint("Ex: Unicamp");
        mapsActivity.showInfoWindow(latLng);
    }

    private void hideKeyboard(){
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void updateResultSearch(List<List<HashMap<String, String>>> predictions) {
        searchBar.clearSuggestions();
        this.predictions = predictions;
        List<Place> list = new ArrayList<>();
        if(predictions!= null){
            for(List<HashMap<String, String>> l: predictions){
                String s = l.get(0).get("description");
                String t = l.get(2).get("secondary_text");
                if(s!= null && t!=null)
                    list.add(new Place(s,t));
            }
        }
        customSuggestionsAdapter.setSuggestions(list);
        searchBar.setCustomSuggestionAdapter(customSuggestionsAdapter);
        searchBar.updateLastSuggestions(list);
    }

    @Override
    public void onSearchStateChanged(boolean enabled) {
        if(mapsActivity.isControlButtonsVisible() && enabled)
            mapsActivity.showControlButtons(false);
        else
            mapsActivity.showControlButtons(true);
    }

    public void updatePlaceDetails(String name, String address, String website,String phone) {
        mapsActivity.updatePlaceDetails(name,address,website,phone);
    }
}
