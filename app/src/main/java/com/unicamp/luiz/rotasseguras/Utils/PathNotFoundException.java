package com.unicamp.luiz.rotasseguras.Utils;

public class PathNotFoundException extends Exception {

    public PathNotFoundException() {
        super("Path from source to destination vertex was not found");
    }
}
