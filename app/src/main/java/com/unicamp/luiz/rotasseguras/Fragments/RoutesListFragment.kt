package com.unicamp.luiz.rotasseguras.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.model.LatLng
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity
import com.unicamp.luiz.rotasseguras.R

class RoutesListFragment : Fragment(), View.OnClickListener {

    private var routes: ArrayList<ArrayList<LatLng>>? = null
    private var distances: ArrayList<String>? = null
    private var times: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            routes = it.getSerializable("routes") as java.util.ArrayList<java.util.ArrayList<LatLng>>
            distances = it.getSerializable("distances") as ArrayList<String>
            times = it.getSerializable("times") as ArrayList<String>
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_routes_list, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)

        if (recyclerView is RecyclerView) {
            with(recyclerView) {
                layoutManager = LinearLayoutManager(context)
                adapter = RoutesViewAdapter(distances!!, times!!, this@RoutesListFragment)
            }
        }
        return view
    }

    override fun onClick(v: View?) {
        (activity?.supportFragmentManager?.findFragmentByTag("MapsFragment") as MapsActivity).getRoutes(v?.tag as Int)
        activity?.supportFragmentManager?.popBackStack()
    }
}
