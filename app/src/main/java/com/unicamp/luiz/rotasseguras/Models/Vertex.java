package com.unicamp.luiz.rotasseguras.Models;

public class Vertex<T> {

    private final T payload;
    private final int totalIncidents;

    public Vertex(T payload, int totalIncidents) {
        this.payload = payload;
        this.totalIncidents = totalIncidents;
    }

    public T getPayload() {
        return payload;
    }

    @Override
    public boolean equals(Object other) {
        try {
            return equals((Vertex) other);
        } catch (ClassCastException e) {
            return false;
        }
    }

    public int getTotalIncidents(){
        return totalIncidents;
    }

    public boolean equals(Vertex other) {
        return payload.equals(other.getPayload());
    }

    @Override
    public int hashCode() {
        return payload.hashCode();
    }

    @Override
    public String toString() {
        return payload.toString();
    }

}
