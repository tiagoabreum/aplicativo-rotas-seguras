package com.unicamp.luiz.rotasseguras.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ListView;

import com.unicamp.luiz.rotasseguras.R;
import com.unicamp.luiz.rotasseguras.Utils.StepsAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class StepsListActivity extends AppCompatActivity {


    private List<ArrayList<HashMap<String, String>>> steps;
    private ListView listView;
    private StepsAdapter list_adapter;
    private ArrayList <String> stepsInfo;
    private ArrayList <String> stepsTime;
    private ArrayList<Integer> stepsImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps_list);
        Intent intent = getIntent();
        int currentRoute = intent.getIntExtra("currentRoute", 0);
        initializeArray();
        steps = (List<ArrayList<HashMap<String, String>>>) intent.getSerializableExtra("steps");
        getSteps(currentRoute);
        init();
        listView.setAdapter(list_adapter);
    }

    private void initializeArray(){
        stepsInfo = new ArrayList<>();
        stepsTime = new ArrayList<>();
        stepsImages = new ArrayList<>();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Etapas de navegação");
        list_adapter = new StepsAdapter(this,stepsInfo, stepsImages,stepsTime);
        listView = findViewById(R.id.lv_languages);
    }

    private void getSteps(int routeSelected){

        if(steps==null || steps.size()==0)
            return;

        String time;
        String distance;
        String info;
        String text;

        if(routeSelected>= steps.size())
            routeSelected = steps.size()-1;

        ArrayList<HashMap<String,String>> a = steps.get(routeSelected);

        for(int j=0;j<a.size();j++){
            info = a.get(j).get("instruction");
            text = String.valueOf(Html.fromHtml(info));
            time = a.get(j).get("time");
            distance = a.get(j).get("distance");
            text+= " ("+distance+")";

            stepsTime.add(time);
            stepsInfo.add(text);

            if(text.contains("esquerda"))
                stepsImages.add(R.drawable.baseline_arrow_back_black_48);
            else if(text.contains("direita"))
                stepsImages.add(R.drawable.baseline_arrow_forward_black_48);
            else if(text.contains("ª"))
                stepsImages.add(R.drawable.baseline_rotate_right_black_48);
            else
                stepsImages.add(R.drawable.baseline_arrow_upward_black_48);
        }
    }

}
