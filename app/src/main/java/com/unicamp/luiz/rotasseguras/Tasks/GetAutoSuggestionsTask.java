package com.unicamp.luiz.rotasseguras.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.unicamp.luiz.rotasseguras.R;
import com.unicamp.luiz.rotasseguras.Utils.DirectionsJSONParser;
import com.unicamp.luiz.rotasseguras.Utils.SearchAutoComplete;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GetAutoSuggestionsTask extends AsyncTask<String, Void, String>{


    private static Context context;
    private static SearchAutoComplete searchAutoComplete;
    private final int typeSearch;
    private static final String TAG = GetAutoSuggestionsTask.class.getName();
    private final int SEARCH_FROM_SEARCH_BAR = 0;

    public GetAutoSuggestionsTask(Context context,SearchAutoComplete searchAutoComplete, int typeSearch){
        GetAutoSuggestionsTask.context = context;
        GetAutoSuggestionsTask.searchAutoComplete = searchAutoComplete;
        this.typeSearch = typeSearch;
    }


    @Override
    protected String doInBackground(String...place) {
        String data = "";
        String url;
        if(typeSearch == SEARCH_FROM_SEARCH_BAR)
            url = requestUrl(place[0]);
        else
            url = place[0];
        try {
            data = DownloadTask.downloadUrl(url);
        } catch (Exception e) {
            Log.d("Background Task", e.toString());
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        JSONObject jObject;
        List<List<HashMap<String, String>>> predictions = new ArrayList<>();
        LatLng latLng = null;

        if(typeSearch == SEARCH_FROM_SEARCH_BAR){
            try {
                jObject = new JSONObject(result);
                predictions = DirectionsJSONParser.parsePlace(jObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            if(predictions!=null && predictions.size()>0)
                requestPlaceWithId(predictions.get(0).get(1).get("place_id"));

            searchAutoComplete.updateResultSearch(predictions);
        }else{
            try {
                jObject = new JSONObject(result);
                latLng = DirectionsJSONParser.parsePlaceWithId(jObject,this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            searchAutoComplete.updateReturnWithId(latLng);
        }
        this.cancel(true);
    }

    private String requestUrl(String s){
        String key = "key="+context.getResources().getString(R.string.google_maps_key);
        String input="";
        String language = "language=pt-Br";
        try {
            input = "input=" + URLEncoder.encode(s, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String parameters = input+"&"+key +"&components=country:BR"+"&"+language;
        String output = "json";
        return "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;
    }

    public static String requestPlaceWithId(String s){
        String key = "key="+context.getResources().getString(R.string.google_maps_key);
        String language = "language=pt-Br";
        String place_id = "placeid="+s;
        String parameters = language+"&"+place_id+"&"+key;
        String output = "json";
        String result = "https://maps.googleapis.com/maps/api/place/details/"+output+"?"+parameters;

        searchAutoComplete.updateId(result);

        return result;
    }

    public static String getDirectionsUrl(LatLng origin, LatLng dest, ArrayList<LatLng> waypoints, int typeSearch) {

        String key = "key="+context.getResources().getString(R.string.google_maps_key);

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String mode = "mode=driving";

        //Alternative routes enabled in common search
        if(typeSearch == 0)
            key = key + "&alternatives=true";


        String language = "language=pt-Br";


        StringBuilder waypointsString= new StringBuilder();
        /*StringBuilder waypointsString;*/
        if (waypoints!= null && waypoints.size() > 0) {
            waypointsString = new StringBuilder("&waypoints=optimize:true|");
            for (int i = 1; i < waypoints.size(); i++) {
                waypointsString.append(waypointsString.toString().equals("") ? "" : "%7C").append(waypoints.get(i).latitude).append(",").append(waypoints.get(i).longitude);
            }
        }

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" +
                mode + "&" +language +  "&" + key ;

        if(!waypointsString.toString().equals("")){
            parameters += "&" +waypointsString;
        }
        // Output format
        String output = "json";

        // Building the url to the web service

        Log.d(TAG,"https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters);

        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }

        public static String getDirectionsForIntent(ArrayList<LatLng> waypoints) {
            String uri = "";
            for (LatLng latLng : waypoints) {
                if (TextUtils.isEmpty(uri)) {
                    uri = String.format(
                            "http://maps.google.com/maps?saddr=%s, %s",
                            String.valueOf(latLng.latitude).replace(",", "."),
                            String.valueOf(latLng.longitude).replace(",", ".")
                    );
                } else {
                    if (!uri.contains("&daddr")) {
                        uri += String.format(
                                "&daddr=%s, %s",
                                String.valueOf(latLng.latitude).replace(",", "."),
                                String.valueOf(latLng.longitude).replace(",", ".")
                        );
                    } else {
                        uri += String.format(
                                "+to:%s, %s",
                                String.valueOf(latLng.latitude).replace(",", "."),
                                String.valueOf(latLng.longitude).replace(",", ".")
                        );
                    }
                }
            }

            return uri;


        }



        public void updatePlaceDetails(String name, String address, String website,String phone) {
        searchAutoComplete.updatePlaceDetails(name,address,website,phone);
    }
}
