package com.unicamp.luiz.rotasseguras.Fragments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.unicamp.luiz.rotasseguras.R

class RoutesViewAdapter(
        private val mDistances: ArrayList<String>,
        private val mTimes: ArrayList<String>,
        private val mFragment: RoutesListFragment)
    : RecyclerView.Adapter<RoutesViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_routes_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val distance = mDistances[position]
        val time = mTimes[position]
        holder.mDistance.text = distance
        holder.mTime.text = time
        holder.mRouteName.text = "Rota " + (position+1)

        with(holder.mView) {
            tag = position
            setOnClickListener(mFragment)
        }
    }

    override fun getItemCount(): Int = mDistances.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mDistance: TextView = mView.findViewById(R.id.textViewDistance)
        val mTime: TextView = mView.findViewById(R.id.textViewTime)
        val mRouteName: TextView = mView.findViewById(R.id.routeName)
    }
}
