package com.unicamp.luiz.rotasseguras.Tasks;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

// Fetches data from url passed
public class DownloadTask extends AsyncTask<String, Void, Void> {

    @SuppressLint("StaticFieldLeak")
    private final MapsActivity mapsActivity;
    private final DownloadTask downloadTask;
    private final int seeach_type;

    public DownloadTask(MapsActivity mapsActivity, int seeach_type){
        this.mapsActivity = mapsActivity;
        downloadTask = this;
        this.seeach_type = seeach_type;
    }

    public static String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Error", e.toString());
        } finally {
            Objects.requireNonNull(iStream).close();
            Objects.requireNonNull(urlConnection).disconnect();
        }
        return data;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        downloadTask.cancel(true);
    }

    @Override
    protected Void doInBackground(String... url) {
        try {
            ParserTask parserTask = new ParserTask(mapsActivity,seeach_type);
            parserTask.execute(downloadUrl(url[0]));
        } catch (Exception e) { Log.d("Background Task", e.toString()); }
        return null;
    }

}
