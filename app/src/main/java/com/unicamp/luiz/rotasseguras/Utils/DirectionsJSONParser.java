package com.unicamp.luiz.rotasseguras.Utils;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;
import com.unicamp.luiz.rotasseguras.Tasks.GetAutoSuggestionsTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DirectionsJSONParser {

    private final MapsActivity mapsActivity;
    private final int SEARCH_SAFEST_ROUTE = 1;

    public DirectionsJSONParser(MapsActivity mapsActivity){
        this.mapsActivity = mapsActivity;
    }



    /** Receives a JSONObject and returns a list of lists containing latitude and longitude */
    public static List<List<HashMap<String, String>>> parsePlace(JSONObject jObject) {
        List<List<HashMap<String, String>>> predictions = new ArrayList<>();
        JSONArray jPredictions;
        String jDescription;
        String jSubDescription;
        String jPlaceID;


        try
        {
            jPredictions = jObject.getJSONArray("predictions");
            List<HashMap<String, String>> path;

            for (int i = 0; i < jPredictions.length(); i++)
            {

                path = new ArrayList<>();

                jDescription   = ((JSONObject)jPredictions.get(i)).getJSONObject("structured_formatting").getString("main_text");
                jSubDescription = "";
                if(((JSONObject)jPredictions.get(i)).getJSONObject("structured_formatting").has("secondary_text")){
                    jSubDescription   = ((JSONObject)jPredictions.get(i)).getJSONObject("structured_formatting").getString("secondary_text");
                }
                jPlaceID  = ((JSONObject)jPredictions.get(i)).getString("place_id");

                HashMap<String, String> hashMapDescription = new HashMap<>();
                hashMapDescription.put("description", jDescription);

                HashMap<String, String> hashMapSecondary = new HashMap<>();
                hashMapSecondary.put("secondary_text", jSubDescription);

                HashMap<String, String> hashMapPlaceId = new HashMap<>();
                hashMapPlaceId.put("place_id", jPlaceID);

                path.add(hashMapDescription);
                path.add(hashMapPlaceId);
                path.add(hashMapSecondary);
                predictions.add(path);
            }

            } catch (Exception e){
                e.printStackTrace();
            }
            return predictions;
    }

    public static LatLng parsePlaceWithId(JSONObject jObject, GetAutoSuggestionsTask getAutoSuggestionsTask) {
        LatLng latLng = null;
        List<List<HashMap<String, String>>> predictions = new ArrayList<>();
        JSONArray placeId = null;
        String website = null;
        String phone = null;
        String address = null;
        String name = null;

        try
        {
            JSONObject result = jObject.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
            Double longitude  = result.getDouble("lng");
            Double latitude =  result.getDouble("lat");

            result = new JSONObject(jObject.toString()).getJSONObject("result");

            if( result.has("name"))
                name = result.getString("name");

            if( result.has("formatted_address"))
                address = result.getString("formatted_address");

            if( result.has("website"))
                website = result.getString("website");

            if( result.has("formatted_phone_number"))
                phone = result.getString("formatted_phone_number");

            getAutoSuggestionsTask.updatePlaceDetails(name,address,website,phone);

            latLng = new LatLng(latitude,longitude);

        } catch (Exception e){
            e.printStackTrace();
        }

        return latLng;
    }

     public List<List<HashMap<String, String>>> parse(JSONObject jObject, int search_type) {
         List<List<HashMap<String, String>>> routes = new ArrayList<>();
         List <ArrayList<HashMap<String, String>>> steps = new ArrayList<>();
         JSONArray jRoutes;
         JSONArray jLegs;
         JSONArray jSteps;
         JSONObject jDistance;
         JSONObject jDuration;
         int safeDistance= 0;
         int safeDuration= 0;

         String TAG = "DirectionsJSONParser";
         Log.d(TAG, jObject.toString());
         try
         {
             jRoutes = jObject.getJSONArray("routes");
             for (int i = 0; i < jRoutes.length(); i++)
             {
                 jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                 List<HashMap<String, String>> path = new ArrayList<>();
                 for (int j = 0; j < jLegs.length(); j++) {
                     jDistance = ((JSONObject) jLegs.get(j)).getJSONObject("distance");
                     int FINAL_SEARCH_REQUEST = 2;
                     if(search_type == FINAL_SEARCH_REQUEST){
                         safeDistance += jDistance.getInt("value");
                     }
                     HashMap<String, String> hmDistance = new HashMap<>();
                     hmDistance.put("distance", jDistance.getString("text"));

                     jDuration = ((JSONObject) jLegs.get(j)).getJSONObject("duration");
                     if(search_type == FINAL_SEARCH_REQUEST){
                         safeDuration += jDuration.getInt("value");
                     }
                     HashMap<String, String> hmDuration = new HashMap<>();
                     hmDuration.put("duration", jDuration.getString("text"));

                     if(j == 0){
                        path.add(hmDistance);
                        path.add(hmDuration);
                     }
                     if(j== jLegs.length()-1 && (search_type == FINAL_SEARCH_REQUEST)){
                         mapsActivity.updateSafestDistance(safeDistance);
                         mapsActivity.updateSafestDuration(safeDuration);
                     }

                     jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");


                     ArrayList<HashMap<String, String>> stepsList = new ArrayList<>();
                     for (int k = 0; k < jSteps.length(); k++) {
                         String polyline;

                         String distance;
                         distance = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("distance")).get("text");

                         String time;
                         time = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("duration")).get("text");

                         String html_instructions;
                         html_instructions = (String) ((JSONObject) jSteps.get(k)).get("html_instructions");

                         HashMap<String, String> stepsHM = new HashMap<>();
                         stepsHM.put("time", time);
                         stepsHM.put("distance", distance);
                         stepsHM.put("instruction", html_instructions);
                         stepsList.add(stepsHM);


                         polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                         List<LatLng> list = this.decodePoly(polyline);

                         for (int l = 0; l < list.size(); l++) {
                             HashMap<String, String> hm = new HashMap<>();
                             hm.put("lat", Double.toString((list.get(l)).latitude));
                             hm.put("lng", Double.toString((list.get(l)).longitude));
                             path.add(hm);
                         }
                     }
                     if(search_type == FINAL_SEARCH_REQUEST){
                         mapsActivity.adiciona_steps_parciais_rota_segura(stepsList);
                     }
                     steps.add(stepsList);
                 }
                 routes.add(path);
             }

         } catch (Exception e){
             e.printStackTrace();
         }
         int COMMON_SEARCH = 0;
         if(search_type == COMMON_SEARCH)
             mapsActivity.updateSteps(steps);
         return routes;
     }


     public void geoJsonParser(int sector){
         ArrayList<String> placesIdHashKey = new ArrayList<>();
         InputStream resourceReader = mapsActivity.getContext().getResources().openRawResource(sector);
         Writer writer = new StringWriter();
         try {
             BufferedReader reader = new BufferedReader(new InputStreamReader(resourceReader, "UTF-8"));
             String line = reader.readLine();
             while (line != null) {
                 writer.write(line);
                 line = reader.readLine();
             }
         } catch (Exception e) {
             e.printStackTrace();
         } finally {
             try {
                 resourceReader.close();
             } catch (Exception e) {
                    e.printStackTrace();
             }
         }

         try {
              JSONObject jsonObject = new JSONObject(writer.toString());
              JSONArray features = jsonObject.getJSONArray("features");
              for (int i=0; i< features.length();i++) {
                  JSONObject geometry = ((JSONObject) features.get(i)).getJSONObject("geometry");
                  String type = geometry.getString("type");
                  JSONArray coordinates = (JSONArray) geometry.get("coordinates");
                  JSONArray coordinateValues = (JSONArray) coordinates.get(0);
                  String privateID = (String) ((JSONObject) features.get(i)).getJSONObject("properties").get("id");
                  int level =  ((JSONObject) features.get(i)).getJSONObject("properties").getInt("level");
                  placesIdHashKey.add(privateID);
                  ArrayList<LatLng> latLngArrayList = new ArrayList<>();
                  if(type.equals("Polygon")){
                      Double latAuxM =0.0;
                      Double lngAuxM = 0.0;
                      int j;
                      for(j=0; j< coordinateValues.length();j++){
                          JSONArray latLngVal = (JSONArray) coordinateValues.get(j);
                              try{
                                  Double lat = latLngVal.getDouble(1);
                                  latAuxM += lat;
                                  Double lon = latLngVal.getDouble(0);
                                  lngAuxM += lon;
                                  LatLng latLng = new LatLng(lat, lon);
                                  latLngArrayList.add(latLng);
                              }catch (Exception e){
                                  e.printStackTrace();
                              }
                      }
                      mapsActivity.atualiza_HashMap_Setores_pontoMedio_para_RayCast(privateID, new LatLng(latAuxM/j,lngAuxM/j));
                      JSONArray latlngMed = (JSONArray) coordinateValues.get(coordinateValues.length() / 2);
                      Double latMedium = latlngMed.getDouble(1);
                      Double lonMedium = latlngMed.getDouble(0);
                      mapsActivity.atualiza_HashMap_Setores_pontoMedio(privateID, new LatLng(latMedium,lonMedium));
                  }else if(type.equals("MultiPolygon")){
                      Double latAuxM =0.0;
                      Double lngAuxM = 0.0;
                      int j=0,k=0;
                      for(j=0; j<coordinates.length();j++){
                          coordinateValues = (JSONArray) ((JSONArray) coordinates.get(j)).get(0);
                          for(k=0; k< coordinateValues.length();k++){
                              JSONArray latlngMulti = (JSONArray) coordinateValues.get(k);
                              Double lat = latlngMulti.getDouble(1);
                              latAuxM += lat;
                              Double lon = latlngMulti.getDouble(0);
                              lngAuxM += lon;
                              LatLng latLng = new LatLng(lat, lon);
                              latLngArrayList.add(latLng);
                          }
                      }

                      mapsActivity.atualiza_HashMap_Setores_pontoMedio_para_RayCast(privateID, new LatLng(latAuxM/(j*k),lngAuxM/(j*k)));

                      try {
                          coordinateValues = (JSONArray) ((JSONArray) coordinates.get(coordinates.length() / 2)).get(0);
                          int medium = coordinateValues.length()/2;
                          JSONArray latlngMed = (JSONArray) ((JSONArray)((JSONArray) coordinates.get(coordinates.length() / 2)).get(0)).get(medium);
                          Double latMedium = latlngMed.getDouble(1);
                          Double lonMedium = latlngMed.getDouble(0);
                          mapsActivity.atualiza_HashMap_Setores_pontoMedio(privateID, new LatLng(latMedium,lonMedium));
                      }catch (Exception e){
                          e.printStackTrace();
                      }

                  }

                  mapsActivity.atualiza_Nivel_Perigo_Setores_Censitarios(privateID, level);
                  mapsActivity.atualiza_HashMap_Setores_Censitarios_Pontos(privateID, latLngArrayList);

              }
         } catch (Exception e) {
             e.printStackTrace();
         }
            mapsActivity.atualiza_Lista_de_Setores(placesIdHashKey);
     }

     private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len)
        {
            int b, shift = 0, result = 0;
            do
            {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do
            {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng(((lat / 1E5)), ((lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }
}