package com.unicamp.luiz.rotasseguras.Tasks;

import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.TypedValue;

import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;
import com.unicamp.luiz.rotasseguras.R;
import com.unicamp.luiz.rotasseguras.Utils.DirectionsJSONParser;

import java.lang.reflect.Field;

public class SetoresCensitariosTask extends AsyncTask<Void, Void, Void>{

    private final String TAG = SetoresCensitariosTask.class.getName();
    private final MapsActivity mapsActivity;

    public SetoresCensitariosTask(MapsActivity mapsActivity){
        this.mapsActivity = mapsActivity;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        DirectionsJSONParser directionsJSONParser = new DirectionsJSONParser(mapsActivity);
        if(isStoragePermissionGranted()){
            Field[] fields=R.raw.class.getFields();
            if(fields!=null && fields.length>0){
                for (Field field : fields) {
                    String filename = field.getName();
                    int rawId = mapsActivity.getResources().getIdentifier(filename, "raw", mapsActivity.getContext().getPackageName());
                    TypedValue value = new TypedValue();
                    mapsActivity.getResources().getValue(rawId, value, true);
                    if (value.toString().contains(".geojson")) {
                        Log.d(TAG, filename);
                        directionsJSONParser.geoJsonParser(rawId);
                    }
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Log.d(TAG,"Todos os setores já estão configurados...");
        this.cancel(true);
    }

    private boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (mapsActivity.getContext().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(mapsActivity.getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }
}
