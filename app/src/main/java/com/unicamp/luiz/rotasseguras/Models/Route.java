package com.unicamp.luiz.rotasseguras.Models;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

public class Route implements Serializable {

    private ArrayList<ArrayList<LatLng>> routes;
    private transient int currentRoute;
    private transient int numRoutes;
    private ArrayList<String> distance;
    private ArrayList<String> time;

    public ArrayList<String> getDistance() {
        return distance;
    }

    public void setDistance(ArrayList<String> distance) {
        this.distance = distance;
    }

    public ArrayList<String> getTime() {
        return time;
    }

    public void setTime(ArrayList<String> time) {
        this.time = time;
    }

    public Route(){
        routes  = new ArrayList<>();
        distance = new ArrayList<>();
        time = new ArrayList<>();
        this.numRoutes = 0;
        this.currentRoute = 0;
    }

    public void addRoute(ArrayList<LatLng> points, String distance,String time){
        this.routes.add(points);
        this.distance.add(distance);
        this.time.add(time);
        this.numRoutes++;
    }

    public ArrayList<LatLng>  getRoute() {
        if (currentRoute < numRoutes) {
            return this.routes.get(currentRoute);
        }else
            return null;
    }

    public String getCurrentDistance(){
        if(currentRoute < distance.size()){
            return distance.get(currentRoute);
        }else
            return null;
    }

    public String getCurrentDuration() {
        if(currentRoute < time.size()){
            return time.get(currentRoute);
        }else
            return null;
    }

    public void setRoute(int position){
        currentRoute = position;
    }

    public int getCurrentRoute(){
        return currentRoute;
    }

    public int getNumRoutes(){
        return numRoutes;
    }

    public ArrayList<ArrayList<LatLng>> getRoutes() {
        return routes;
    }

    public void setRoutes(ArrayList<ArrayList<LatLng>> routes) {
        this.routes = routes;
    }

}
