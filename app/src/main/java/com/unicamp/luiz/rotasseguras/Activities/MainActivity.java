package com.unicamp.luiz.rotasseguras.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.unicamp.luiz.rotasseguras.R;
import com.unicamp.luiz.rotasseguras.Tasks.SubsectorTask;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RapidFloatingActionContentLabelList.OnRapidFloatingActionContentLabelListListener{

    private final String TAG = this.getClass().getName();
    private static final int  REQUEST_ACCESS_FINE_LOCATION = 111, REQUEST_WRITE_STORAGE = 112;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        checkForGooglePlayServices();
        getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));

        FloatingActionButton zoomIn = this.findViewById(R.id.zoomIn);
        FloatingActionButton zoomOut = this.findViewById(R.id.zoomOut);
        FloatingActionButton cl = this.findViewById(R.id.current_location);
        RapidFloatingActionLayout rfaLayout = this.findViewById(R.id.layer_fab_layout);
        RapidFloatingActionButton rfaBtn = this.findViewById(R.id.layer_fab);

        zoomIn.setVisibility(View.GONE);
        zoomOut.setVisibility(View.GONE);
        cl.setVisibility(View.GONE);

        RapidFloatingActionContentLabelList rfaContent = new RapidFloatingActionContentLabelList(this);
        rfaContent.setOnRapidFloatingActionContentLabelListListener(this);
        List<RFACLabelItem> items = new ArrayList<>();
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Level 7")
                .setResId(R.drawable.transparent)
                .setIconNormalColor(ContextCompat.getColor(this, R.color.plevel7))
                .setWrapper(0)
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Level 6")
                .setResId(R.drawable.transparent)
                .setIconNormalColor(ContextCompat.getColor(this, R.color.plevel6))
                .setWrapper(1)
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Level 5")
                .setResId(R.drawable.transparent)
                .setIconNormalColor(ContextCompat.getColor(this, R.color.plevel5))
                .setWrapper(2)
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Level 4")
                .setResId(R.drawable.transparent)
                .setIconNormalColor(ContextCompat.getColor(this, R.color.plevel4))
                .setWrapper(3)
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Level 3")
                .setResId(R.drawable.transparent)
                .setIconNormalColor(ContextCompat.getColor(this, R.color.plevel3))
                .setWrapper(4)
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Level 2")
                .setResId(R.drawable.transparent)
                .setIconNormalColor(ContextCompat.getColor(this, R.color.plevel2))
                .setWrapper(5)
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel("Level 1")
                .setResId(R.drawable.transparent)
                .setIconNormalColor(ContextCompat.getColor(this, R.color.plevel1))
                .setWrapper(6)
        );

        rfaContent.setItems(items);

        RapidFloatingActionHelper rfabHelper = new RapidFloatingActionHelper(
                this,
                rfaLayout,
                rfaBtn,
                rfaContent
        ).build();

        boolean hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

        boolean hasPermissionWrite = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        if (!hasPermissionLocation) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        }


        if (!hasPermissionWrite) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }

        if(hasPermissionLocation && hasPermissionWrite){
            MapsActivity mapsActivity = new MapsActivity();

            android.support.v4.app.FragmentManager fragmentManager = this.getSupportFragmentManager();

            android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();

            transaction.add(R.id.container, mapsActivity, "MapsFragment").addToBackStack(null);

            transaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish();
                    startActivity(getIntent());
                } else {
                    Toast.makeText(MainActivity.this, "" + "Permissôes de acesso são necessárias. Tente novamente!", Toast.LENGTH_LONG).show();
                }
            }

            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish();
                    startActivity(getIntent());
                } else {
                    Toast.makeText(MainActivity.this, "Permissôes de acesso são necessárias. Tente novamente!", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    private boolean checkForGooglePlayServices() {
        Dialog errorDialog;
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                errorDialog = googleApiAvailability.getErrorDialog(this, resultCode, 2404);
                errorDialog.setCancelable(false);
                if (!errorDialog.isShowing())
                    errorDialog.show();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            findViewById(R.id.zoomIn).setVisibility(View.VISIBLE);
            findViewById(R.id.zoomOut).setVisibility(View.VISIBLE);
            findViewById(R.id.current_location).setVisibility(View.VISIBLE);
            findViewById(R.id.searchBar).setVisibility(View.VISIBLE);
            findViewById(R.id.card_view_distance).setVisibility(View.VISIBLE);
            findViewById(R.id.layer_fab_card).setVisibility(View.VISIBLE);
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onRFACItemLabelClick(int position, RFACLabelItem item) {
    }

    @Override
    public void onRFACItemIconClick(int position, RFACLabelItem item) {
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
}
