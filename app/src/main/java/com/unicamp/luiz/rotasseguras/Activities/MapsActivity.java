package com.unicamp.luiz.rotasseguras.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolylineOptions;
import com.unicamp.luiz.rotasseguras.Models.CoordinateIncidents;
import com.unicamp.luiz.rotasseguras.Models.Polyline;
import com.unicamp.luiz.rotasseguras.Models.Route;
import com.unicamp.luiz.rotasseguras.R;
import com.unicamp.luiz.rotasseguras.Tasks.DownloadTask;
import com.unicamp.luiz.rotasseguras.Tasks.GetAutoSuggestionsTask;
import com.unicamp.luiz.rotasseguras.Tasks.ParserTask;
import com.unicamp.luiz.rotasseguras.Tasks.RayCastAlgorithmTask;
import com.unicamp.luiz.rotasseguras.Tasks.SetoresCensitariosTask;
import com.unicamp.luiz.rotasseguras.Utils.CustomProgressBar;
import com.unicamp.luiz.rotasseguras.Utils.SearchAutoComplete;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MapsActivity extends SupportMapFragment implements LocationListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMapLoadedCallback, OnMapReadyCallback, GoogleMap.InfoWindowAdapter, GoogleMap.OnMapClickListener {

    private final String TAG = MapsActivity.class.getName();
    private ArrayList<LatLng> points, waypoints, waypointsSafeRoute, newRequestPoints, partialPoints, markerPoints;
    private boolean controlButtons = true, rota_segura_auxiliar = false, searchCompleted = false, searching = false;
    private LatLng currentPos, destination, originForSafestPath, destination_safeRoute;
    private GoogleMap mMap;
    private Context context;
    private Activity activity;
    private TextView tvDistanceDuration, tvDistanceTime;
    private CardView cardView;
    private Route routes;
    private List <ArrayList<HashMap<String, String>>> steps;
    private String name, address, website, phone;
    private FloatingActionButton zoomIn, zoomOut, currentLocationButton ;
    private final int FINAL_SEARCH_REQUEST = 2;
    private static boolean syncSafestPath = true;
    private int currentIndex = 0, safestDistance = 0, safestDuration = 0;
    private ArrayList<HashMap<String, String>> stepsInParts;
    public static final CustomProgressBar progressBar = new CustomProgressBar();
    private ArrayList<String> placesHashKey = new ArrayList<>();
    private HashMap<String, ArrayList<LatLng>> setoresCensitarios;
    private HashMap<String, LatLng> setoresPontoMedio, setoresPontoMedioRaycast;
    private HashMap<String, Integer> dangerPlacesHash;
    private ArrayList<Polygon> lista_poligonos;
    private BroadcastReceiver listener;
    private final IntentFilter filter = new IntentFilter("rota_segura");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        context = activity.getApplicationContext();
        tvDistanceDuration = activity.findViewById(R.id.route_distance);
        tvDistanceTime = activity.findViewById(R.id.route_time);
        cardView = activity.findViewById(R.id.card_view_distance);
        markerPoints = new ArrayList<>();
        zoomIn = activity.findViewById(R.id.zoomIn);
        zoomOut = activity.findViewById(R.id.zoomOut);
        currentLocationButton = activity.findViewById(R.id.current_location);
        points = new ArrayList<>();
        this.getContext().registerReceiver(listener, filter);
        processSetoresCensitarios();
        getMapAsync(this);
        enableControlButtons();
    }

    private void enableControlButtons() {
        FloatingActionButton zoomIn = activity.findViewById(R.id.zoomIn);
        FloatingActionButton zoomOut = activity.findViewById(R.id.zoomOut);
        FloatingActionButton cl = activity.findViewById(R.id.current_location);

        zoomIn.setVisibility(View.VISIBLE);
        zoomOut.setVisibility(View.VISIBLE);
        cl.setVisibility(View.VISIBLE);


        zoomIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
            }
        });

        zoomOut.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
            }
        });

        cl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentPos!=null)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPos,16f));
            }
        });
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final MapsActivity mapsActivity = this;
        listener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getBooleanExtra("rota_principal", false)){
                    Toast.makeText(context, "Rota principal é a rota mais segura", Toast.LENGTH_LONG).show();
                }
                else if(progressBar.getDialog()!=null && progressBar.getDialog().isShowing()){
                    progressBar.getDialog().dismiss();
                    int safeRoute = mapsActivity.getRoute().getNumRoutes()-1;
                    if(safeRoute>0 && safeRoute!= routes.getCurrentRoute())
                        mapsActivity.getRoutes(safeRoute);
                }
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(listener, filter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(listener);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location !=null ){
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16f));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new SearchAutoComplete(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(this);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.style_json));
        mMap.setOnMapClickListener(this);
        this.getMap().setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if(routes!=null && routes.getNumRoutes()>0 && lista_poligonos!=null && lista_poligonos.size()>0){
                    LatLngBounds latLngBounds = mMap.getProjection().getVisibleRegion().latLngBounds;
                    for(Polygon p: lista_poligonos){
                        for(LatLng l: p.getPoints()){
                            if(latLngBounds.contains(l)){
                                p.setVisible(true);
                                break;
                            }else{
                                p.setVisible(false);
                            }
                        }
                    }
                }
            }
        });
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        assert locationManager != null;
        locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        Location location;
        location= locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if(location==null) //Try with gps
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location==null)
            location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (location != null) {
            onLocationChanged(location);
            currentPos = new LatLng(location.getLatitude(),location.getLongitude());
            if(markerPoints!=null)
                markerPoints.add(new LatLng(location.getLatitude(),location.getLongitude()));
        }

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        mMap.setOnMapLoadedCallback(this);
        updateMarkers();
        if (markerPoints.size() >= 2) {
            searchCompleted = false;
            LatLng origin = markerPoints.get(0);
            LatLng dest = markerPoints.get(1);
            int COMMON_SEARCH = 0;
            String url = GetAutoSuggestionsTask.getDirectionsUrl(dest, origin, null, COMMON_SEARCH);
            searching = true;
            DownloadTask downloadTask = new DownloadTask(this, COMMON_SEARCH);
            downloadTask.execute(url);
        }
        if(marker!=null)
            marker.hideInfoWindow();
        showControlButtons(true);
    }

    @Override
    public void onMapLoaded() {
        if(currentPos!=null && destination!=null){
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(currentPos).zoom(17)
                    .bearing(90).tilt(40).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            try {
                mMap.animateCamera(CameraUpdateFactory.newLatLng(currentPos),3000,
                        new GoogleMap.CancelableCallback() {
                            @Override
                            public void onFinish() {
                            }

                            @Override
                            public void onCancel() {
                            }
                        });
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public View getInfoWindow(Marker marker) {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert layoutInflater != null;
            @SuppressLint("InflateParams") View v = layoutInflater.inflate(R.layout.info_window, null);

            TextView name = v.findViewById(R.id.name);
            TextView address = v.findViewById(R.id.address);
            TextView phone = v.findViewById(R.id.phone);
            TextView website = v.findViewById(R.id.website);

            if (this.name != null) {
                name.setText(this.name);
            }else{
                name.setVisibility(View.GONE);
            }

            if (this.address != null) {
                address.setText(this.address);
            }else{
                address.setVisibility(View.GONE);
            }

            if (this.phone != null) {
                phone.setText(this.phone);
            }else{
                phone.setVisibility(View.GONE);
            }

            if (this.website != null) {
                website.setText(this.website);
            }else{
                website.setVisibility(View.GONE);
            }

            showControlButtons(false);

            return v;
        }catch (Exception e){
            return null;
        }
    }


    public void getRoutes(int position) {
        getActivity().findViewById(R.id.zoomIn).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.zoomOut).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.current_location).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.searchBar).setVisibility(View.VISIBLE);

        if(routes==null)
            return;
        else
            routes.setRoute(position);
        if(routes.getCurrentRoute() >= routes.getNumRoutes()){
            Toast.makeText(context, "Nenhuma nova rota pode ser encontrada ", Toast.LENGTH_LONG).show();
        }else{
            if(mMap!=null)
                mMap.clear();
            updateMarkers();
            new RayCastAlgorithmTask(this, FINAL_SEARCH_REQUEST).execute(routes.getRoute());
             // for(LatLng l: waypointsSafeRoute)CoordinateIncidents.showIcon(l,2, this);
            cardView.setVisibility(View.VISIBLE);
            activity.findViewById(R.id.layer_fab_card).setVisibility(View.VISIBLE);
            String textDistance = routes.getCurrentDistance();
            getActivity().findViewById(R.id.card_view_distance).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.layer_fab_card).setVisibility(View.VISIBLE);
            tvDistanceDuration.setText(textDistance);
            tvDistanceTime.setText(routes.getCurrentDuration());
            Toast.makeText(context, "Nova Rota selecionada", Toast.LENGTH_LONG).show();
            if(points!=null){
                points.clear();
            }
            points = (ArrayList<LatLng>) routes.getRoute().clone();
            PolylineOptions polylineOptions;
            polylineOptions = Polyline.setPolyOptions(points);
            mMap.addPolyline(polylineOptions);
        }
    }

    public void updateRoutes(Route routes) {
        if(routes!= null){
            this.routes = routes;
            if(routes.getCurrentDuration()!=null && routes.getCurrentDistance()!=null){
                cardView.setVisibility(View.VISIBLE);
                activity.findViewById(R.id.layer_fab_card).setVisibility(View.VISIBLE);
                String textDistance = routes.getCurrentDistance();
                tvDistanceDuration.setText(textDistance);
                tvDistanceTime.setText(routes.getCurrentDuration());
                cardView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity,StepsListActivity.class);
                        intent.putExtra("steps", (Serializable) getSteps()); //Optional parameters
                        intent.putExtra("currentRoute",getRoute().getCurrentRoute());
                        activity.startActivity(intent);
                    }
                });
            }
        }
    }

    public void adiciona_rota_segura() {
        if(rota_segura_auxiliar){
            Log.d(TAG, " (ROTA SEGURA) Rota segura adicionada...");
            Double safeDouble = (double) safestDistance;
            DecimalFormat df = new DecimalFormat("#.#");
            String distance = String.valueOf(df.format(safeDouble/1000)) + " Km";
            searchCompleted = true;
            String duration = adjustTimeUnit(safestDuration);
            if(partialPoints!=null)
                this.routes.addRoute(new ArrayList<>(partialPoints),distance,duration);
            adiciona_steps_rota_segura();
            Intent intent = new Intent("rota_segura");
            LocalBroadcastManager.getInstance(this.getContext()).sendBroadcast(intent);
            searching = false;
            if(partialPoints!=null)
                partialPoints.clear();
            if(stepsInParts!=null)
                stepsInParts.clear();
            rota_segura_auxiliar = false;
            currentIndex=0;
            syncSafestPath = true;
            if(waypoints!=null)
                waypoints.clear();
            if(newRequestPoints!=null)
                newRequestPoints.clear();
        }else{
            rota_segura_auxiliar = true;
            syncSafestPath = true;
            currentIndex=0;
            waypointsSafeRoute = new ArrayList<>();
            waypointsSafeRoute.add(currentPos);
            if(newRequestPoints!=null && newRequestPoints.size()>0){
                waypointsSafeRoute.addAll(newRequestPoints);
                LatLng last = newRequestPoints.get(newRequestPoints.size()-1);
                Double distance = CoordinateIncidents.distance(destination.latitude, destination.longitude,
                        last.latitude, last.longitude);
                if(distance >= 50.0)
                    waypointsSafeRoute.add(destination);
            }
            waypointsSafeRoute.addAll(newRequestPoints);
            if(newRequestPoints.size()>1){
                LatLng last = newRequestPoints.get(newRequestPoints.size()-1);
                Double distance = CoordinateIncidents.distance(destination.latitude, destination.longitude,
                    last.latitude, last.longitude);
                if(distance >= 50.0)
                    waypointsSafeRoute.add(destination);
            }
            if(stepsInParts!=null)
                stepsInParts.clear();
            if(waypoints!=null)
                waypoints.clear();
            if(newRequestPoints!=null)
                newRequestPoints.clear();
            if(partialPoints!=null)
                partialPoints.clear();
            safestDistance = 0;
            safestDuration = 0;
            requisicao_pontos_rota_segura(waypointsSafeRoute,FINAL_SEARCH_REQUEST);
        }
    }

    public void mapsIntentRequest() {
        if(waypointsSafeRoute!=null && waypointsSafeRoute.size()>1){
            waypointsSafeRoute.set(1, destination);
            waypointsSafeRoute.remove(waypointsSafeRoute.size()-1);
            final String uri = GetAutoSuggestionsTask.getDirectionsForIntent(waypointsSafeRoute);
            final Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }

    public void requisicao_pontos_rota_segura(ArrayList<LatLng> p, int search_type) {
        if(currentIndex==0) originForSafestPath = getCurrentPos();
        if(search_type == FINAL_SEARCH_REQUEST){
            if (currentIndex<p.size() && syncSafestPath) {
                Log.d(TAG," (ROTA SEGURA) Buscando por rota segura...");
                ArrayList<LatLng> pontos = wayPoints_rota_segura(p);
                processa_rota_segura(originForSafestPath, destination_safeRoute, FINAL_SEARCH_REQUEST, pontos);
            }
        }else{
            if (currentIndex<p.size()-1 && syncSafestPath) {
                currentIndex += 1;
                Log.d(TAG," (ROTA SEGURA) Etapa da requisicao:"+currentIndex);
                int SEARCH_SAFEST_ROUTE = 1;
                processa_rota_segura(originForSafestPath,p.get(currentIndex), SEARCH_SAFEST_ROUTE, null);
            }
        }
    }

    private ArrayList<LatLng> wayPoints_rota_segura(ArrayList<LatLng> p){
        if(p!=null && p.size()>0){
            ArrayList<LatLng> waypointsList = new ArrayList<>();
            int i=currentIndex;
            int counter=0;
            int MAX_WAYPOINTS = 23;
            while(counter!= MAX_WAYPOINTS){
                if(i<p.size()){
                    waypointsList.add(p.get(i));
                    counter++;i++;
                    currentIndex = i;
                }else{
                    break;
                }
            }
            if(i<p.size()) destination_safeRoute = p.get(i);
            else destination_safeRoute = p.get(i-1);

            if(p.size()<=MAX_WAYPOINTS){
                waypointsList.remove(waypointsList.size()-1);
            }

            return waypointsList;
        }
        return null;
    }

    private void processa_rota_segura(LatLng origem,LatLng dest, int search_type, ArrayList<LatLng> waypoints){
        Log.d(TAG,"Proceding to safest route...");
        String url = GetAutoSuggestionsTask.getDirectionsUrl(origem, dest, waypoints, search_type);
        originForSafestPath = dest;
        searching  = true;
        DownloadTask downloadTask = new DownloadTask(this,search_type);
        downloadTask.execute(url);
        syncSafestPath = false;
    }

    public void adiciona_pontos_parciais_rota_segura(ArrayList<LatLng> points) {
        ArrayList<LatLng> pontos_necessarios= new ArrayList<>();
        if(partialPoints==null){
            partialPoints = new ArrayList<>();
            newRequestPoints = new ArrayList<>();
        }

        if(points!=null && points.size()>0){
            int counter=0, counter2 = 0;
            for(LatLng l: points){
                if(partialPoints!=null && partialPoints.size()>0){
                    for(LatLng l2: partialPoints){
                        Double distance = CoordinateIncidents.distance(l.latitude, l.longitude, l2.latitude, l2.longitude);
                        if(distance<20){
                            counter2++;
                        }
                    }
                }
            }

            LatLng lastCommonPoint = null;
            if(partialPoints!=null){
                for(LatLng l: points){
                    if(partialPoints.contains(l)){
                        partialPoints.remove(l);
                        lastCommonPoint = l;
                        counter++;
                    }
                    else{
                        pontos_necessarios.add(l);
                    }
                }
            }
            if(lastCommonPoint!= null && counter2<points.size()/2){
                Log.d(TAG,"CONTADOR:"+counter);
                Log.d(TAG,"CONTADOR 2:"+counter2);
                if(counter2<3 && counter<3)
                    newRequestPoints.add(lastCommonPoint);
            }
            if(pontos_necessarios.size()>0)
                partialPoints.addAll(pontos_necessarios);
        }
    }


    private void adiciona_steps_rota_segura() {
        if(stepsInParts!=null && steps!=null)
            steps.add(new ArrayList<>(stepsInParts));
        if(stepsInParts!=null)
            stepsInParts.clear();
    }

    public void adiciona_steps_parciais_rota_segura(ArrayList<HashMap<String, String>> stepsParts) {
        if(stepsInParts==null)
            stepsInParts = new ArrayList<>();
        if(stepsParts!=null && stepsParts.size()>0)
            stepsInParts.addAll(stepsParts);
    }

    public  void sincroniza_rota_segura (boolean val, ParserTask parserTask, int search_type){
        syncSafestPath = val;
        calcelParserTask(parserTask);
        if(search_type == FINAL_SEARCH_REQUEST)
            requisicao_pontos_rota_segura(waypointsSafeRoute,search_type);
        else
            requisicao_pontos_rota_segura(waypoints,search_type);
    }

    public void atualiza_Lista_de_Setores(ArrayList<String> placesHashKey){
        if(this.placesHashKey == null || this.placesHashKey.size()==0)
            this.placesHashKey = placesHashKey;
        else{
            for(String s: placesHashKey){
                if(!this.placesHashKey.contains(s))
                    this.placesHashKey.add(s);
            }
        }
    }

    public void atualiza_HashMap_Setores_Censitarios_Pontos(String s, ArrayList<LatLng> a) {
        if(setoresCensitarios == null) {
            setoresCensitarios = new HashMap<>();
            setoresCensitarios.put(s,a);
        }else{
            if(!setoresCensitarios.containsKey(s))
                setoresCensitarios.put(s,a);
        }
    }

    public void atualiza_HashMap_Setores_pontoMedio(String s, LatLng l) {
        if(setoresPontoMedio == null){
            setoresPontoMedio = new HashMap<>();
            setoresPontoMedio.put(s,l);
        }
        else{
            if(!setoresPontoMedio.containsKey(s))
                setoresPontoMedio.put(s,l);
        }
    }

    public void atualiza_HashMap_Setores_pontoMedio_para_RayCast(String s, LatLng l) {
        if(setoresPontoMedioRaycast == null){
            setoresPontoMedioRaycast = new HashMap<>();
            setoresPontoMedioRaycast.put(s,l);
        }
        else{
            if(!setoresPontoMedioRaycast.containsKey(s))
                setoresPontoMedioRaycast.put(s,l);
        }
    }

    public void atualiza_Nivel_Perigo_Setores_Censitarios(String privateID, int level) {
        if(dangerPlacesHash == null){
            dangerPlacesHash = new HashMap<>();
            dangerPlacesHash.put(privateID,level);
        }
        else{
            if(!dangerPlacesHash.containsKey(privateID))
                dangerPlacesHash.put(privateID,level);
        }
    }

    public void showInfoWindow(LatLng markerPos) {
        if(markerPos!=null){
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(markerPos);
            Marker marker = mMap.addMarker(markerOptions);
            marker.showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(markerPos));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16F));
            destination = markerPos;
        }
    }

    public void updatePlaceDetails(String name, String address, String website,String phone) {
        this.address = address;this.name = name;
        this.website = website;this.phone = phone;
    }

    public void showControlButtons(boolean visibility) {
        int visible;
        if(visibility) visible = View.VISIBLE;
        else visible = View.INVISIBLE;
        zoomIn.setVisibility(visible);
        currentLocationButton.setVisibility(visible);
        zoomOut.setVisibility(visible);
        if(cardView.getVisibility()==View.GONE && !tvDistanceTime.getText().equals("")){
            cardView.setVisibility(View.VISIBLE);
            activity.findViewById(R.id.layer_fab_card).setVisibility(View.VISIBLE);
        }else{
            cardView.setVisibility(View.GONE);
            activity.findViewById(R.id.layer_fab_card).setVisibility(View.GONE);
        }
        controlButtons = visibility;
    }

    private void updateMarkers() {
        if(currentPos!=null && destination!=null){
            if(markerPoints!=null)
                markerPoints.clear();
            else
                markerPoints = new ArrayList<>();
            markerPoints.add(destination);
            markerPoints.add(currentPos);

            MarkerOptions dest = new MarkerOptions();
            dest.position(destination);
            dest.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

            MarkerOptions currentPosition = new MarkerOptions();
            currentPosition.position(currentPos);
            currentPosition.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));

            currentPos = currentPosition.getPosition();
            mMap.addMarker(currentPosition);
            mMap.addMarker(dest);
        }
    }

    private String adjustTimeUnit (int seconds){
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
        String s = "";
        if(day!=0){
            if(day == 1)
                s += day+ " dia ";
            else
                s += day+ " dias ";
            s += hours+ " horas";
            return  s;
        }
        if(hours!=0){
            if(hours == 1)
                s+= hours + " hora ";
            else
                s += hours+ " horas ";
        }
        if(minute!=0)
            if(minute == 1)
                s += minute+ " minuto";
            else
                s += minute+ " minutos";
        return s;
    }

    public void atualiza_lista_de_poligonos(Polygon p) {
        if(lista_poligonos == null)
            lista_poligonos = new ArrayList<>();
        lista_poligonos.add(p);
    }

    public void atualiza_rota_segura() {
        Log.d(TAG, "Rota principal é a mais segura...");
        Intent intent = new Intent("rota_segura");
        intent.putExtra("rota_principal", true);
        searching= false;
        LocalBroadcastManager.getInstance(this.getContext()).sendBroadcast(intent);
    }

    public boolean isSearchCompleted(){ return  searchCompleted;}

    public boolean isSearching() { return searching; }

    private void processSetoresCensitarios() { new SetoresCensitariosTask(this).execute(); }

    @Override
    public Context getContext() { return context; }

    public GoogleMap getMap(){ return this.mMap; }

    public void updateWaypoints (ArrayList<LatLng> p){ waypoints = p; }

    public Route getRoute(){return routes;}

    public List <ArrayList<HashMap<String, String>>> getSteps(){ return steps;}

    public boolean isControlButtonsVisible() { return controlButtons; }

    public HashMap<String, ArrayList<LatLng>> getSetoresCensitarios() { return setoresCensitarios; }

    public HashMap<String, LatLng> getHashMap_setores_pontoMedio_Raycast() { return setoresPontoMedioRaycast; }

    public HashMap<String, LatLng> getHashMap_setores_pontoMedio() { return setoresPontoMedio; }

    private void calcelParserTask(ParserTask parserTask){ parserTask.cancel(true); }

    public void updateSafestDistance(int safeDistance) { safestDistance += safeDistance; }

    public void updateSafestDuration(int safeDuration) { safestDuration += safeDuration; }

    public HashMap<String, Integer> recupera_Nivel_De_Perigo_Setores_Censitarios() { return dangerPlacesHash; }

    public ArrayList<String> getPlacesHashKey() { return placesHashKey; }

    public LatLng getDestination() { return destination; }

    private LatLng getCurrentPos() { return currentPos; }

    public void updateSteps(List<ArrayList<HashMap<String, String>>> steps) { this.steps = steps; }

    @Override
    public void onMapClick(LatLng latLng) { showControlButtons(true); }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) { }

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) { }

    @Override
    public View getInfoContents(Marker marker) { return null; }
}
