package com.unicamp.luiz.rotasseguras.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.unicamp.luiz.rotasseguras.Activities.StepsListActivity;
import com.unicamp.luiz.rotasseguras.R;

import java.util.ArrayList;

public class StepsAdapter extends BaseAdapter{
    private final ArrayList<String> result;
    private final ArrayList<String> time;
    private final Context context;
    private final ArrayList<Integer> imageId;
    private static LayoutInflater inflater=null;

    public StepsAdapter(StepsListActivity stepsListActivity, ArrayList<String> list, ArrayList<Integer> images, ArrayList<String> timeSteps) {
        result=list;
        context= stepsListActivity;
        time = timeSteps;
        imageId=images;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class Holder {
        TextView textView;
        TextView textViewTime;
        ImageView imageView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        @SuppressLint("ViewHolder") View view =inflater.inflate(R.layout.layout_steps_list, null);
        holder.textView= view.findViewById(R.id.textViewSteps);
        holder.textViewTime= view.findViewById(R.id.textViewStepsTime);
        holder.imageView= view.findViewById(R.id.imageViewSteps);
        holder.textView.setText(result.get(position));
        holder.textViewTime.setText(time.get(position));
        Picasso.with(context).load(imageId.get(position)).into(holder.imageView);

        return view;
    }

}