package com.unicamp.luiz.rotasseguras.Models;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;

public class CoordinateIncidents {

        public static Double distance (double lat_a, double lng_a, double lat_b, double lng_b ) {
            double earthRadius = 3958.75;
            double latDiff = Math.toRadians(lat_b-lat_a);
            double lngDiff = Math.toRadians(lng_b-lng_a);
            double a = Math.sin(latDiff /2) * Math.sin(latDiff /2) +
                    Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                            Math.sin(lngDiff /2) * Math.sin(lngDiff /2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            double distance = earthRadius * c;

            int meterConversion = 1609;

        return distance * meterConversion;
    }

    /*FOR TESTING */
    public static void showIcon(LatLng l, final int color, final MapsActivity mapsActivity){
        final LatLng u = l;
        mapsActivity.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                MarkerOptions c = new MarkerOptions();
                c.position(u);
                switch (color) {
                    case 1:
                        c.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        break;
                    case 2:
                        c.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                        break;
                    default:
                        c.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                        break;
                }
                mapsActivity.getMap().addMarker(c);
            }
        });
    }/*FOR TESTING */
}
