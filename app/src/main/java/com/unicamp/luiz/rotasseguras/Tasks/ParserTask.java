package com.unicamp.luiz.rotasseguras.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;
import com.unicamp.luiz.rotasseguras.Models.CoordinateIncidents;
import com.unicamp.luiz.rotasseguras.Models.Polyline;
import com.unicamp.luiz.rotasseguras.Models.Route;
import com.unicamp.luiz.rotasseguras.Utils.DirectionsJSONParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

    private final MapsActivity mapsActivity;
    private final String TAG = ParserTask.class.getName();
    private Polyline polyline;
    private Route routes;
    private final int search_type;

    public ParserTask(MapsActivity mapsActivity, int seeach_type){
        this.mapsActivity = mapsActivity;
        this.search_type = seeach_type;
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

        JSONObject jObject;
        List<List<HashMap<String, String>>> listRoutes = null;

        try {
            jObject = new JSONObject(jsonData[0]);
            DirectionsJSONParser parser = new DirectionsJSONParser(mapsActivity);
            listRoutes = parser.parse(jObject,search_type);
        } catch (Exception e) { e.printStackTrace(); }

        polyline = new Polyline(mapsActivity);

        routes  = new Route();

        return listRoutes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {

        if(result==null)
            return;

        int possibleRoutes = result.size();

        Log.d(TAG,"NUM_ROUTES:"+result.size());

        for(int i = 0; i< possibleRoutes; i++){
            routes.addRoute(new ArrayList<>(Objects.requireNonNull(Polyline.getPoints(result, i))), polyline.getDistance(), polyline.getDuration());
            Log.d("TAG", polyline.getDistance());
        }
        if(routes!=null){
            ArrayList<LatLng> points = routes.getRoute();
            if(points!=null){
                int COMMON_SEARCH = 0;
                int FINAL_SEARCH_REQUEST = 2;
                int SEARCH_SAFEST_ROUTE = 1;
                if(search_type == COMMON_SEARCH){
                    Polyline.showPolyline(points,mapsActivity);
                    mapsActivity.updateRoutes(routes);
                new RayCastAlgorithmTask(mapsActivity, COMMON_SEARCH).execute(points);
                }else if(search_type == SEARCH_SAFEST_ROUTE || search_type == FINAL_SEARCH_REQUEST){
                    if(points.size()>1){
                        LatLng last = points.get(points.size()-1);
                        LatLng dest = mapsActivity.getDestination();
                        Double distance = CoordinateIncidents.distance(last.latitude, last.longitude, dest.latitude, dest.longitude);
                        mapsActivity.adiciona_pontos_parciais_rota_segura(points);
                        Log.d(TAG, "(ROTA SEGURA) Distancia:"+distance);
                        Double MIN_DISTANCE = 100.0;
                        if(distance<= MIN_DISTANCE){
                            Log.d(TAG,"Buscando rota segura... "+points.size());
                            mapsActivity.adiciona_rota_segura();
                        }else{
                            Log.d(TAG," (ROTA SEGURA) Adicionando rota parcial...");
                            mapsActivity.sincroniza_rota_segura(true,this, search_type);
                        }
                    }else{
                        Log.d(TAG,"(ROTA SEGURA) Falhou -> (Pontos) "+ (points != null ? points.size() : 0));
                    }
                    this.cancel(true);
                }
            }
        }
    }
}
