package com.unicamp.luiz.rotasseguras.Models;

import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Polyline {

        private static final int width = 10;
        private static final int color = Color.rgb(0,179,253);
        private static ArrayList<LatLng> points;
        private static String distance = "";
        private static String duration = "";
    private static GoogleMap mMap;

        public Polyline(MapsActivity mapsActivity){
            points = new ArrayList<>();
            mMap = mapsActivity.getMap();
        }

        private static boolean verifyResult(List<List<HashMap<String, String>>> result){
            return result == null || result.size() >= 1;
        }

        //Route selected
        public static ArrayList<LatLng> getPoints(List<List<HashMap<String, String>>> result, int route){
            if(points!=null)
                points.clear();
            else
                points = new ArrayList<>();
            if(verifyResult(result)){
                List<HashMap<String, String>> path = result.get(route);
                if(path!=null){
                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        if (j == 0) {    // Get distance from the list
                            distance = point.get("distance");
                            continue;
                        } else if (j == 1) { // Get duration from the list
                            duration = point.get("duration");
                            continue;
                        }
                        try {
                            double lat = Double.parseDouble(point.get("lat"));
                            double lng = Double.parseDouble(point.get("lng"));
                            LatLng position = new LatLng(lat, lng);
                            points.add(position);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    return points;
                }else
                    return null;
            }else
                return null;
        }

        public String getDistance(){ return distance; }

        public String getDuration() { return duration; }

        public static PolylineOptions setPolyOptions(ArrayList<LatLng> points){
                PolylineOptions lineOptions = new PolylineOptions();
            try {
                lineOptions.addAll(points)
                        .geodesic(true)
                        .color(color)
                        .width(width)
                        .startCap(new   RoundCap())
                        .endCap(new RoundCap());
                        return lineOptions;
                }catch (Exception e){
                    e.printStackTrace();
                    return null;
                }
        }

        public static void showPolyline(final ArrayList<LatLng> points, MapsActivity mapsActivity) {
            new DynamicallyAddPolyline(mapsActivity).execute(points);
        }

        private static class DynamicallyAddPolyline extends AsyncTask<ArrayList<LatLng>, Void, PolylineOptions>{

            private final MapsActivity mapsActivity;
            DynamicallyAddPolyline(MapsActivity mapsActivity) {
                this.mapsActivity = mapsActivity;
            }

            @SafeVarargs
            protected final PolylineOptions doInBackground(ArrayList<LatLng>... latlng) {
                return Polyline.setPolyOptions(latlng[0]);
            }

            protected void onPostExecute(final PolylineOptions result) {
                if(result!= null){
                    mMap.addPolyline(result).setJointType(JointType.ROUND);
                }
                this.cancel(true);
            }
    }
}
