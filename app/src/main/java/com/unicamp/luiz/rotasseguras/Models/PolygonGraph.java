package com.unicamp.luiz.rotasseguras.Models;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;
import com.unicamp.luiz.rotasseguras.Tasks.SubsectorTask;
import com.unicamp.luiz.rotasseguras.Utils.DjikstraPolygonFind;
import com.unicamp.luiz.rotasseguras.Utils.PathNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class PolygonGraph {

    private final MapsActivity mapsActivity;
    private static final String TAG = PolygonGraph.class.toString();
    private List<Vertex> vertexList;
    private List<Edge> edgeList;

    public PolygonGraph(MapsActivity mapsActivity) {
        this.mapsActivity = mapsActivity;
    }

    public void drawSubSector(ArrayList<String> keys) {
       new SubsectorTask(mapsActivity, keys).execute();
    }

    public void executeGraph(ArrayList<LatLng> latLng, HashMap<LatLng, String> ponto_setor){
        Log.d(TAG, "Executando Grafo");
        if(latLng!=null && latLng.size()>0)
            new GraphRun(mapsActivity, ponto_setor).execute(latLng);
    }

    private class GraphRun extends AsyncTask< ArrayList<LatLng>, Void, ArrayList<ArrayList<LatLng>>> {

        private ArrayList<LatLng> result;
        private final MapsActivity mapsActivity;
        private ArrayList<String> keys;
        private HashMap<String,LatLng> setorPontoMedio, setorPontoMedioRaycast;
        private final HashMap<LatLng,String> ponto_setor;
        private HashMap<String, Integer> setorPerigo;
        private LatLng destination;
        private Double maxDistance = 500.0;


        GraphRun(final MapsActivity mapsActivity, HashMap<LatLng, String> ponto_setor) {
            this.mapsActivity = mapsActivity;
            this.ponto_setor = ponto_setor;
        }
        @SafeVarargs
        protected final ArrayList<ArrayList<LatLng>> doInBackground(ArrayList<LatLng>... latlng) {
            Log.d(TAG, "(ROTA SEGURA) maxDistace: "+maxDistance);
            result = latlng[0];
            if (result==null || result.size()==0)
                this.cancel(true);
            if(result.size()==2){ //Rota principal já eh a mais segura
                mapsActivity.atualiza_rota_segura();
                this.cancel(true);
            }
            vertexList = new ArrayList<>();
            keys = mapsActivity.getPlacesHashKey();
            setorPontoMedio = mapsActivity.getHashMap_setores_pontoMedio();
            setorPontoMedioRaycast = mapsActivity.getHashMap_setores_pontoMedio_Raycast();
            setorPerigo = mapsActivity.recupera_Nivel_De_Perigo_Setores_Censitarios();
            if(setorPerigo!=null)
                setorPerigo.put("0",0); //Para os pontos de inicio e fim como areas sem risco -- eh preciso passar por eles
            destination=mapsActivity.getDestination();
            if(keys!=null)
                Log.d(TAG, "Numero de setores: "+keys.size());
            Log.d(TAG, "Numero de pontos a serem analisados (inicio): "+result.size());


            try {
                buildEdges(vertexList);
            } catch (PathNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute( ArrayList<ArrayList<LatLng>> polygon) {
            this.cancel(true);
        }

        private void buildEdges(List<Vertex> vertexList) throws PathNotFoundException {
            edgeList = new ArrayList<>();

            Log.d(TAG, "(ROTA SEGURA) Construindo adjascencias...");

            //Para o caminho padrão...garantindo no minimo a rota principal
            if(result!=null && result.size()>0){
                for (int i=0;i<result.size()-1;i++){
                        int j = i+1;
                        if(j<result.size()){
                            LatLng p1 = result.get(i);
                            LatLng p2 = result.get(j);
                            Double distancia = CoordinateIncidents.distance(p1.latitude,p1.longitude, p2.latitude, p2.longitude);
                            Double distanciaDoDestino = CoordinateIncidents.distance(destination.latitude,destination.longitude, (p1.latitude+p2.latitude)/2, (p1.longitude+p2.longitude)/2);
                            createEdge(result.get(i),result.get(j), distancia, distanciaDoDestino);
                        }
                }

                if(edgeList!=null)
                    Log.d(TAG, "(ROTA SEGURA) Numero de adjascencias (inicio): "+edgeList.size());

                //buscando setores vizinhos
                ArrayList<LatLng> vizinhos = new ArrayList<>();
                for (int i=1;i<result.size();i++){
                    LatLng ponto = result.get(i);
                    if(keys!=null && setorPontoMedio!=null && setorPontoMedioRaycast!=null){
                        for(int j=0; j< keys.size(); j++){
                            String s = keys.get(j);
                            LatLng p = setorPontoMedioRaycast.get(s);
                            LatLng p1 = setorPontoMedio.get(s);
                            Double distancia = CoordinateIncidents.distance(ponto.latitude,ponto.longitude, p.latitude, p.longitude);
                            Double distanciaDoDestino = CoordinateIncidents.distance(destination.latitude,destination.longitude, (ponto.latitude+p1.latitude)/2, (ponto.longitude+p1.longitude)/2);
                            if(distancia<=maxDistance){
                                if(ponto_setor!=null)
                                    ponto_setor.put(p1,s);
                                vizinhos.add(p1);
                                createEdge(ponto,p1, distancia, distanciaDoDestino);
                                //CoordinateIncidents.showIcon(p, 3, mapsActivity);
                            }
                        }
                    }
                }
                Log.d(TAG, "(ROTA SEGURA) Numero de adjascencias (c/ vizinhos): "+edgeList.size());


                //Para os novos vizinhos, busca uma nova vizinhaca ---> 2 niveis
                ArrayList<LatLng> newVizinhos = new ArrayList<>();
                if(keys!=null && setorPontoMedio!=null && setorPontoMedioRaycast!=null){
                    for(int i =0;i<=1;i++){
                        for(LatLng l: vizinhos){
                            for(int j=0; j< keys.size(); j++){
                                String s = keys.get(j);
                                LatLng p = setorPontoMedio.get(s);
                                LatLng p1 = setorPontoMedioRaycast.get(s);
                                if(l == p)
                                    continue;
                                Double distancia = CoordinateIncidents.distance(l.latitude,l.longitude, p1.latitude, p1.longitude);
                                Double distanciaDoDestino = CoordinateIncidents.distance(destination.latitude,destination.longitude, (p.latitude+l.latitude)/2, (l.longitude+p.longitude)/2);
                                if(distancia<=maxDistance && !vizinhos.contains(p) && !vertexList.contains(p)){
                                    if(ponto_setor!=null)
                                        ponto_setor.put(p,s);
                                    newVizinhos.add(p);
                                    createEdge(l,p, distancia, distanciaDoDestino);
                                }
                            }
                        }
                        Log.d(TAG, "(ROTA SEGURA) Numero de adjascencias nivel("+i+") :"+edgeList.size());
                        vizinhos.clear();
                        vizinhos.addAll(newVizinhos);
                    }
                }

                //evitando que setores facam loops incoerentes
                final LatLng endPoint= result.get(result.size()-1);
                for (Vertex v: vertexList) {
                    LatLng v1 = (LatLng) v.getPayload();
                    for (Vertex v_2: vertexList) {
                        LatLng v2 = (LatLng) v_2.getPayload();
                        if(v1!=endPoint){
                            Double distance = CoordinateIncidents.distance(v1.latitude, v1.longitude, v2.latitude, v2.longitude);
                            Double distanciaDoDestino = CoordinateIncidents.distance(destination.latitude,destination.longitude, (v1.latitude+v2.latitude)/2, (v1.longitude+v2.longitude)/2);
                            if(distance <= maxDistance){
                                createEdge(v1, v2, distance, distanciaDoDestino);
                            }
                        }
                    }
                }
                Log.d(TAG, "Numero de adjascencias final (ROTA SEGURA):"+edgeList.size());
                //for(Vertex v: vertexList){CoordinateIncidents.showIcon((LatLng)v.getPayload(), 3, mapsActivity);}

                buildGraph(vertexList,edgeList);
            }
        }

        private void createEdge(LatLng i, LatLng j, Double distance, Double distanciaDoDestino){
            if(ponto_setor!=null && setorPerigo!=null && vertexList!=null && edgeList!=null){
                try {
                    String s = ponto_setor.get(i);
                    String s2 = ponto_setor.get(j);
                    Integer level1 = setorPerigo.get(s);
                    Integer level2 = setorPerigo.get(s2);
                    Vertex<LatLng> start = new Vertex<>(i, level1);
                    Vertex<LatLng> end = new Vertex<>(j, level2);
                    if(!vertexList.contains(start))
                        vertexList.add(start);
                    if(!vertexList.contains(end))
                        vertexList.add(end);
                    Edge edge = new Edge(start,end, (level1+level2)*(int)Math.ceil(Math.log(distanciaDoDestino))*(int)Math.ceil(Math.log(distance)));
                    if(!edgeList.contains(edge))
                        edgeList.add(edge);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        private void buildGraph(List<Vertex> vertexList, List<Edge> edgeList) {
            Log.d(TAG, "Executando Djikstra (ROTA SEGURA) ...");

            try{
                Vertex<LatLng> vertexFrom = vertexList.get(0);
                Vertex<LatLng> vertexTo = new Vertex<>(result.get(result.size() - 1), 0);
                Graph graph = new Graph(edgeList);
                DjikstraPolygonFind djikstraPolygonFind = new DjikstraPolygonFind(graph,result);
                djikstraPolygonFind.execute(vertexFrom);

                int pathWeightedDistance = djikstraPolygonFind.getDistance(vertexTo);

                LinkedList<Vertex> p = djikstraPolygonFind.getPath(vertexTo);
                ArrayList<LatLng>  list = new ArrayList<>();
                for(Vertex v: p){list.add((LatLng)v.getPayload());}

                mapsActivity.updateWaypoints(list);
                mapsActivity.requisicao_pontos_rota_segura(list, 0);
                Log.d(TAG, "Numero de waypoints (ROTA SEGURA) :"+p.size());
                Log.d(TAG,"DJIKSTRA (ROTA SEGURA) -> "+" "+pathWeightedDistance);
            }catch (Exception e){
                mapsActivity.atualiza_rota_segura();
                e.printStackTrace();
            }
        }
    }


}
