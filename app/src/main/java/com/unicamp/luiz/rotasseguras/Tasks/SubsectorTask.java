package com.unicamp.luiz.rotasseguras.Tasks;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;
import com.unicamp.luiz.rotasseguras.R;

import java.util.ArrayList;
import java.util.HashMap;

public class SubsectorTask extends AsyncTask<LatLngBounds, Void, Void>{

    private final String TAG = SubsectorTask.class.getName();
    private final MapsActivity mapsActivity;
    private final ArrayList <String> keys;
    private final HashMap<String,Integer> hash_place_danger;

    public SubsectorTask(MapsActivity mapsActivity, ArrayList<String> keys){
        this.mapsActivity = mapsActivity;
        this.keys = keys;
        this.hash_place_danger = mapsActivity.recupera_Nivel_De_Perigo_Setores_Censitarios();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    protected Void doInBackground(LatLngBounds... bounds) {
        HashMap<String,ArrayList<LatLng>> setores = mapsActivity.getSetoresCensitarios();
        if(keys!=null && keys.size()>0){
            for(String key: keys){
                final int color = getColor(key);
                final PolygonOptions polygonOptions = new PolygonOptions();
                polygonOptions.fillColor(color);
                polygonOptions.strokeWidth(5);
                assert setores.get(key) != null;
                polygonOptions.addAll(setores.get(key));
                mapsActivity.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Polygon p = mapsActivity.getMap().addPolygon(polygonOptions);
                        p.setVisible(false);
                        mapsActivity.atualiza_lista_de_poligonos(p);
                    }
                });
            }
            return null;
        }
        return null;
    }

    private int getColor(String key) {
        try {
            int level = hash_place_danger.get(key);
            switch (level) {
                case 0:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level0);
                case 1:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level1);
                case 2:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level2);
                case 3:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level3);
                case 4:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level4);
                case 5:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level5);
                case 6:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level6);
                default:
                    return ContextCompat.getColor(mapsActivity.getContext(), R.color.level7);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //Return color 0 em caso de problema
        return  ContextCompat.getColor(mapsActivity.getContext(), R.color.level0);
    }

    protected void onPostExecute(Void result) {
        Log.d(TAG, "Subsetores tracados...");
        this.cancel(true);
    }

}
