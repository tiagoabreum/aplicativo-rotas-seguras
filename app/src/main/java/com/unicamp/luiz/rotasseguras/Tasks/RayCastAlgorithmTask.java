package com.unicamp.luiz.rotasseguras.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.unicamp.luiz.rotasseguras.Activities.MapsActivity;
import com.unicamp.luiz.rotasseguras.Models.CoordinateIncidents;
import com.unicamp.luiz.rotasseguras.Models.PolygonGraph;

import java.util.ArrayList;
import java.util.HashMap;

public class RayCastAlgorithmTask extends AsyncTask<ArrayList<LatLng>, Void, Void> {

    private final String TAG = RayCastAlgorithmTask.class.getName();
    private final int typeSearch;
    private final MapsActivity mapsActivity;
    private final ArrayList<LatLng> setoresCensitariosPoints = new ArrayList<>();
    private final PolygonGraph polygonGraph;


    public RayCastAlgorithmTask(MapsActivity mapsActivity, int typeSearch) {
        this.mapsActivity = mapsActivity;
        this.polygonGraph = new PolygonGraph(mapsActivity);
        this.typeSearch = typeSearch;
    }

    private static boolean pointInPolygon(LatLng point, ArrayList<LatLng> polygon) {
        // ray casting alogrithm http://rosettacode.org/wiki/Ray-casting_algorithm
        if(polygon==null || polygon.size() ==0 || point==null)
            return false;

        int crossings = 0;

        // for each edge
        for (int i = 0; i < polygon.size(); i++) {
            LatLng a = polygon.get(i);
            int j = i + 1;
            //to close the last edge, you have to take the first point of your polygon
            if (j >= polygon.size()) {
                j = 0;
            }
            LatLng b = polygon.get(j);
            if (rayCrossesSegment(point, a, b)) {
                crossings++;
            }
        }

        // odd number of crossings?
        return (crossings % 2 == 1);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        this.cancel(true);
    }

    @SafeVarargs
    @Override
    protected final Void doInBackground(ArrayList<LatLng>... list) {
        ArrayList<LatLng> points = list[0];
        if(points==null || points.size()==0)
            return null;
        try {
            ArrayList<String> keys = mapsActivity.getPlacesHashKey();
            HashMap<String, ArrayList<LatLng>> setoresCensitarios = mapsActivity.getSetoresCensitarios();
            ArrayList<String> usedKeys = new ArrayList<>();
            HashMap<LatLng, String> ponto_setor = new HashMap<>();
            HashMap<String, LatLng> setorPontoMedioRaycast = mapsActivity.getHashMap_setores_pontoMedio_Raycast();
            ponto_setor.put(points.get(0), "0");
            LatLng anterior = null;

            for (int i = 0; i< points.size()-1; i++) {
                LatLng ponto = points.get(i);

                if(anterior!= null){
                    Double distance = CoordinateIncidents.distance(ponto.latitude,ponto.longitude,
                            anterior.latitude, anterior.longitude);
                    //Log.d(TAG, "Distancia (" + distance.toString() + ")");
                    if(distance<100.0)
                        continue;
                }
                //Para todos os setores
                for(int j = 0; j< setoresCensitarios.size(); j++) {
                    String s = keys.get(j);
                    //Para todas as chaves
                    if (!usedKeys.contains(s)) {
                        ArrayList <LatLng> subsetor = setoresCensitarios.get(s);
                        LatLng ponto_teste = setorPontoMedioRaycast.get(s);
                        Double distance = CoordinateIncidents.distance(ponto.latitude,
                                ponto.longitude, ponto_teste.latitude, ponto_teste.longitude);
                        //Log.d(TAG, "PONTO (" + i + "): " + distance.toString());
                        if (distance <= 1000) {
                            if (pointInPolygon(ponto, subsetor)) {
                                //Log.d(TAG, "PONTO (" + i + "): ENTROOU com " + distance.toString());
                                setoresCensitariosPoints.add(ponto);
                                usedKeys.add(s);
                                ponto_setor.put(ponto,s);
                                break;
                            }
                        }
                    }
                    anterior = ponto;
                }
            }
            Log.d(TAG, "RayCast completo...");
            ponto_setor.put(points.get(points.size()-1), "0");
            setoresCensitariosPoints.add(points.get(points.size()-1));//adiciona o destino
            int COMMON_SEARCH = 0;
            if(typeSearch == COMMON_SEARCH)
                polygonGraph.executeGraph(setoresCensitariosPoints, ponto_setor);
            polygonGraph.drawSubSector(usedKeys);
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static boolean rayCrossesSegment(LatLng point, LatLng a, LatLng b) {
        // Ray Casting algorithm checks, for each segment, if the point is 1) to the left of the segment and 2) not above nor below the segment. If these two conditions are met, it returns true
        double px = point.longitude,
                py = point.latitude,
                ax = a.longitude,
                ay = a.latitude,
                bx = b.longitude,
                by = b.latitude;
        if (ay > by) {
            ax = b.longitude;
            ay = b.latitude;
            bx = a.longitude;
            by = a.latitude;
        }
        // alter longitude to cater for 180 degree crossings
        if (px < 0 || ax <0 || bx <0) { px += 360; ax+=360; bx+=360; }
        // if the point has the same latitude as a or b, increase slightly py
        if (py == ay || py == by) py += 0.00000001;


        // if the point is above, below or to the right of the segment, it returns false
        if ((py > by || py < ay) || (px > Math.max(ax, bx))){
            return false;
        }
        // if the point is not above, below or to the right and is to the left, return true
        else if (px < Math.min(ax, bx)){
            return true;
        }
        // if the two above conditions are not met, you have to compare the slope of segment [a,b] (the red one here) and segment [a,p] (the blue one here) to see if your point is to the left of segment [a,b] or not
        else {
            double red = (ax != bx) ? ((by - ay) / (bx - ax)) : Double.POSITIVE_INFINITY;
            double blue = (ax != px) ? ((py - ay) / (px - ax)) : Double.POSITIVE_INFINITY;
            return (blue >= red);
        }

    }

}
